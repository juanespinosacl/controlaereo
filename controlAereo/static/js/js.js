$(document).ready(function(){
    $('#fechaVuelo').datepicker({
        language: "es",
        todayBtn: "linked",
        clearBtn: true,
        format: "dd/mm/yyyy",
        multidate: false,
        todayHighlight: true,
        orientation: "bottom left",
        singleDatePicker: true,
        showDropdowns: true,

    });

    $('#datetimepicker').datetimepicker({
        format: 'hh:mm:ss a'
    });
})