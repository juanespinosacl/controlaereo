from controlAereo import db

class Reserva(db.Model):
    __tablename__ = 'reservas'
    idReserva = db.Column(db.Integer, primary_key=True)
    idUsuario = db.Column(db.Integer)
    idVuelo = db.Column(db.Integer)
    
    def __init__(self, idUsuario, idVuelo) -> None:
        self.idUsuario = idUsuario
        self.idVuelo = idVuelo

    def __repr__(self) -> str:
        dataVuelos = {
            'idReserva': self.idReserva,
            'idUsuario': self.idUsuario,
            'idVuelo': self.idVuelo
        }
        return f'{dataVuelos}'