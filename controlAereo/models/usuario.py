from controlAereo import db

class Usuario(db.Model):
    __tablename__ = 'usuario'
    idUsuario = db.Column(db.Integer, primary_key=True)
    nombreCompleto = db.Column(db.String(50))
    email = db.Column(db.String(50))
    celular = db.Column(db.Text)
    password = db.Column(db.Text)
    idTipoUsuario = db.Column(db.Integer)
    estadoUsuario = db.Column(db.Integer)
    
    def __init__(self, nombreCompleto, email, celular, password, idTipoUsuario, estadoUsuario) -> None:
        self.nombreCompleto = nombreCompleto
        self.email = email
        self.celular = celular
        self.password = password
        self.idTipoUsuario = idTipoUsuario
        self.estadoUsuario = estadoUsuario

    def __repr__(self) -> str:
        dataUsuarios = {
            'idUsuario': self.idUsuario,
            'nombreCompleto': self.nombreCompleto,
            'email': self.email,
            'celular': self.celular,
            'idTipoUsuario': self.idTipoUsuario,
            'estadoUsuario': self.estadoUsuario
        }
        return f'{dataUsuarios}'