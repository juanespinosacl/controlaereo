from sqlalchemy.orm import backref
from controlAereo import db

class DatoVuelo(db.Model):
    __tablename__ = 'datos_vuelos'
    idDatoVuelo = db.Column(db.Integer, primary_key=True)
    idAvion = db.Column(db.Integer)
    idPiloto = db.Column(db.Integer)
    estadoVuelo = db.Column(db.String(50))
    idVuelo = db.Column(db.Integer, db.ForeignKey('vuelos.idVuelo'))
    
    def __init__(self, idDatoVuelo, idVuelo, idAvion, idPiloto, estadoVuelo) -> None:
        self.idDatoVuelo = idDatoVuelo
        self.idVuelo = idVuelo
        self.idAvion = idAvion
        self.idPiloto = idPiloto
        self.estadoVuelo = estadoVuelo

    def __repr__(self) -> str:
        return f'Dato Vuelo: {self.idDatoVuelo}'