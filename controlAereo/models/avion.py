from controlAereo import db

class Avion(db.Model):
    __tablename__ = 'avion'
    idAvion = db.Column(db.Integer, primary_key=True)
    referencia = db.Column(db.String(50))
    capacidad = db.Column(db.Integer)
    
    def __init__(self, referencia, capacidad) -> None:
        self.referencia = referencia
        self.capacidad = capacidad

    def __repr__(self) -> str:
        dataAvion = {
            'idAvion': self.idAvion,
            'referencia': self.referencia,
            'capacidad': self.capacidad
        }
        return f'{dataAvion}'