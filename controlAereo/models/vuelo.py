from controlAereo import db

class Vuelo(db.Model):
    __tablename__ = 'vuelos'
    idVuelo = db.Column(db.Integer, primary_key=True)
    origen = db.Column(db.String(50))
    destino = db.Column(db.String(50))
    fecha = db.Column(db.String(50))
    hora = db.Column(db.String(50))
    estado = db.Column(db.String(50))
    idUsuario = db.Column(db.Integer)
    idPiloto = db.Column(db.Integer)
    idAvion = db.Column(db.Integer)
    
    def __init__(self, origen, destino, fecha, hora, estado, idUsuario, idPiloto, idAvion) -> None:
        self.origen = origen
        self.destino = destino
        self.fecha = fecha
        self.hora = hora
        self.estado = estado
        self.idUsuario = idUsuario
        self.idPiloto = idPiloto
        self.idAvion = idAvion

    def __repr__(self) -> str:
        dataVuelos = {
            'idVuelo': self.idVuelo,
            'origen': self.origen,
            'destino': self.destino,
            'estado': self.estado,
            'idPiloto': self.idPiloto,
            'idAvion': self.idAvion
        }
        return f'{dataVuelos}'