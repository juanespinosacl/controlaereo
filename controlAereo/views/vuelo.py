from flask import render_template, Blueprint, flash, g, redirect, request, url_for
from controlAereo.models.vuelo import Vuelo
from controlAereo.models.usuario import Usuario
from controlAereo.models.avion import Avion
from controlAereo.models.reservaVuelo import Reserva
from controlAereo.views.usuario import listarPilotos
from controlAereo.views.auth import login_required
from controlAereo import db

vuelo = Blueprint('vuelo', __name__)

#Listar todos los vuelos
@vuelo.route("/")
def index():
    vuelos = Vuelo.query.all()
    pasajeros = (db.session.query(Usuario, Reserva, Vuelo)).filter(Usuario.idUsuario == Reserva.idUsuario).filter(Reserva.idVuelo == Vuelo.idVuelo).all()
    db.session.commit()
    data = {
        'vuelos': vuelos,
        'pasajeros': list(pasajeros)
    }
    return render_template('controlAereo/index.html', **data)

#crear un vuelo
@vuelo.route("/control/creavuelo", methods=['GET','POST'])
@login_required
def crearVuelo():

    pilotos = listarPilotos()
    aviones = Avion.query.all()

    if request.method == 'POST':
        origen = request.form['origen']
        destino = request.form['destino']
        fecha = request.form['fecha']
        hora = request.form['hora']
        estado = request.form['estado']
        idPiloto = request.form['idPiloto']
        idAvion = request.form['idAvion']

        vuelo = Vuelo(origen, destino, fecha, hora, estado, g.usuario.idUsuario, idPiloto, idAvion)

        dataRespuesta = {
            'respuesta': True
        }

        if dataRespuesta['respuesta']:
            db.session.add(vuelo)
            db.session.commit()
            return redirect(url_for('vuelo.index'))
        else:
            flash(dataRespuesta)

    return render_template('controlAereo/crearVuelo.html', pilotos = pilotos, aviones = aviones)

#crear un vuelo
@vuelo.route("/control/infovuelos", methods=['GET','POST'])
@login_required
def infoVuelos():
    infoVuelos = (db.session.query(Vuelo, Usuario, Avion)).filter(Vuelo.idPiloto == Usuario.idUsuario).filter(Vuelo.idAvion == Avion.idAvion).all()
    return render_template('controlAereo/infoVuelo.html', infoVuelos = infoVuelos)
