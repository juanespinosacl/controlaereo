import functools
from flask import render_template, Blueprint, flash, g, redirect, request, session, url_for
from werkzeug.security import check_password_hash, generate_password_hash
import controlAereo
from controlAereo.models.usuario import Usuario
from controlAereo import db

auth = Blueprint('auth', __name__, url_prefix='/auth')

#Registrar Usuario
@auth.route('/register', methods=['GET','POST'])
def register():
    if request.method == 'POST':
        nombreCompleto = request.form['nombreCompleto']
        email = request.form['email']
        celular = request.form['celular']
        password = request.form['password']
        idTipoUsuario = request.form['idTipoUsuario']
        estadoUsuario = request.form['estadoUsuario']

        usuario = Usuario(nombreCompleto.title(), email, celular, generate_password_hash(password), idTipoUsuario, estadoUsuario)
        usuarioExiste = Usuario.query.filter_by(email = email).first()

        dataRespuesta = {
            'usuario': None,
            'respuesta': False,
        }

        if usuarioExiste:
            dataRespuesta['usuario'] = usuarioExiste
        else:
            db.session.add(usuario)
            db.session.commit()
            dataRespuesta['usuario'] = usuario
            dataRespuesta['respuesta'] = True

        flash(dataRespuesta)

    return render_template('auth/register.html')

#Iniciar Sesión
@auth.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        usuario = Usuario.query.filter_by(email = email).first()

        if usuario and check_password_hash(usuario.password, password):
            if usuario.estadoUsuario == 1:
                session.clear()
                session['idUsuario'] = usuario.idUsuario
                session['nombreUsuario'] = usuario.nombreCompleto
                session['idTipoUsuario'] = usuario.idTipoUsuario
                if usuario.idTipoUsuario == 1:
                    session['tipoUsuario'] = 'Administrador'
                elif usuario.idTipoUsuario == 2:
                    session['tipoUsuario'] = 'Piloto'
                elif usuario.idTipoUsuario == 3:
                    session['tipoUsuario'] = 'Usuario Final'
                session['estadoUsuario'] = usuario.estadoUsuario
                session['estado'] = True
                return redirect(url_for('vuelo.index'))
            else:
                session.clear()
                session['estado'] = False
        else:
            session.clear()
            session['estado'] = False
            
        flash(session)

    return render_template('auth/login.html')

#Comprueba si el usuario tiene sesion activa
@auth.before_app_request
def load_logged_in_user():
    idUsuario = session.get('idUsuario')

    if idUsuario is None:
        g.usuario = None
    else:   
        g.usuario = Usuario.query.get_or_404(idUsuario)

#Cierra sesion
@auth.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('vuelo.index'))

#valida login requerido
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.usuario is None:
            return redirect(url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view



