from flask import render_template, Blueprint, flash, g, redirect, request, url_for
from werkzeug.exceptions import abort
from controlAereo.models.vuelo import Vuelo
from controlAereo.models.usuario import Usuario 
from controlAereo.models.reservaVuelo import Reserva
from controlAereo.models.avion import Avion
from controlAereo.views.auth import login_required
from controlAereo import db

reserva = Blueprint('reserva', __name__)

#Listar todos las reservas
@reserva.route("/control/reserva")
def index():
    reservas = Reserva.query.all()
    db.session.commit()
    return reservas

#crear una reserva
@reserva.route("/control/creareserva", methods=['GET','POST'])
@login_required
def creaReserva():
    vuelos = Vuelo.query.all()
    usuarios = Usuario.query.all()
    db.session.commit()

    if request.method == 'POST':
        idUsuario = request.form['idUsuario']
        idVuelo = request.form['idVuelo']

        reserva = Reserva(idUsuario, idVuelo)

        dataRespuesta = {
            'respuesta': True
        }

        if dataRespuesta['respuesta']:
            db.session.add(reserva)
            db.session.commit()
            return redirect(url_for('vuelo.index'))
        else:
            flash(dataRespuesta)

    data = {
        'vuelos': vuelos,
        'usuarios': usuarios
    }

    return render_template('controlAereo/creaReserva.html', **data)

#informacion de reservas
@reserva.route("/inforeserva/<int:idUsuario>", methods=['GET','POST'])
@login_required
def infoReserva(idUsuario):
    infoReserva = (db.session.query(Reserva, Vuelo, Avion, Usuario)).filter(Reserva.idUsuario == idUsuario, Reserva.idVuelo == Vuelo.idVuelo, Vuelo.idAvion == Avion.idAvion, Vuelo.idPiloto == Usuario.idUsuario).all()
    infoReserva = list(infoReserva)
    return render_template('controlAereo/infoReserva.html', infoReserva = infoReserva)
