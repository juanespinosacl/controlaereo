from flask import render_template, Blueprint, flash, g, redirect, request, url_for
from controlAereo.models.vuelo import Vuelo
from controlAereo.models.usuario import Usuario
from controlAereo.models.reservaVuelo import Reserva
from controlAereo.views.auth import login_required
from controlAereo import db

usuario = Blueprint('usuario', __name__)

def getUsuario(id):
    usuario = Usuario.query.get_or_404(id)
    return usuario

#Listar todos los usuarios
@usuario.route("/control/usuarios")
@login_required
def listarUsuarios():
    usuarios = Usuario.query.all()
    db.session.commit()
    data = {
        'usuarios': usuarios
    }
    return render_template('controlAereo/usuarios.html', **data)

#Listar todos los pilotos
@usuario.route("/control/usuarios")
@login_required
def listarPilotos():
    pilotos = Usuario.query.filter_by(idTipoUsuario = 2).all()
    db.session.commit()
    return pilotos

#Crear usuarios (reutilizado de la creacion de AUTH)
@usuario.route('/control/crearUsuario')
@login_required
def crearUsuario():
     return redirect(url_for('auth.register'))

@usuario.route('/editarusuario/<int:id>', methods=['GET', 'POST'])
@login_required
def editarUsuario(id):
    usuario = getUsuario(id)

    if request.method == 'POST':

        usuario.nombreCompleto = request.form['nombreCompleto']
        usuario.email = request.form['email']
        usuario.celular = request.form['celular']
        usuario.idTipoUsuario = request.form['idTipoUsuario']
        usuario.estadoUsuario = request.form['estadoUsuario']

        dataRespuesta = {
            'usuario': None,
            'respuesta': True,
        }

        db.session.add(usuario)
        db.session.commit()
        dataRespuesta['usuario'] = usuario

        flash(dataRespuesta)

    return render_template('controlAereo/editarUsuario.html', usuario=usuario)




