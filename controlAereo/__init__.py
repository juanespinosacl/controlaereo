from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

#Cargar configuraciones
app.config.from_object('config.DevelopmentConfig')
db = SQLAlchemy(app)

#Importar vistas
from controlAereo.views.auth import auth
app.register_blueprint(auth)

from controlAereo.views.vuelo import vuelo
app.register_blueprint(vuelo)

from controlAereo.views.reservaVuelo import reserva
app.register_blueprint(reserva)

from controlAereo.views.usuario import usuario
app.register_blueprint(usuario)

db.create_all()